{% from "asalae/map.jinja" import asalae with context %}

packages:
  pkg.installed:
     - pkgs: 
       - rsync
       - mailx
       - wget
       - ntp
       - unzip
       - clamav
       - clamav-db

include:
    - postgres
    - postgres.client
    - apache
    - apache.mod_ssl
    - apache.mod_php5
    - apache.mod_rewrite
    - php
    - php.pear
    - php.pgsql
    - php.soap
    - php.xml
    - php.mbstring

asalae_dist:
  archive:
    - name: /opt
    - extracted
    - source: {{ asalae.dist.url }}
    - archive_format: zip
    - source_hash: {{ asalae.dist.hash }}
    - keep: True
    - if_missing: /opt/asalae1.6.0_cakephp1.2.10

/var/www/asalae:
  file.symlink:
    - target: /opt/asalae1.6.0_cakephp1.2.10
    - require:
      - pkg: apache
      - archive: asalae_dist

/opt/asalae1.6.0_cakephp1.2.10:
  file.directory:
    - user: apache
    - group: apache
    - recurse:
      - user
      - group    

{% for cfg in ('asalae.ini.php', 'database.php', 'types_connecteurs.ini.php') %}
/var/www/asalae/app/config/{{ cfg }}:
  file.managed:
    - source: salt://asalae/{{ cfg }}
    - template: jinja
    - require:
      - file: /var/www/asalae
{% endfor %}


XML_RPC:
  pecl.installed:
    - require:
      - pkg: php-pear

/var/www/asalae/cake/console/cake:
 file.managed:
 - mode: 0755

models:
 cmd.run:
 - name: cd /var/www/asalae/app/webroot/files/models && rename _default.odt .odt *.odt

/data/echanges/entree:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/echanges/sortie:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/archives:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/messages:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode


/var/lib/pgsql/.pgpass:
  file.managed:
    - contents: {{ asalae.db.host }}:{{ asalae.db.port }}:{{ asalae.db.database }}:{{ asalae.db.user }}:{{ asalae.db.password }}
    - mode: 0600

/root/.pgpass:
  file.managed:
    - contents: {{ asalae.db.host }}:{{ asalae.db.port }}:{{ asalae.db.database }}:{{ asalae.db.user }}:{{ asalae.db.password }}
    - mode: 0600

/var/www/asalae/app/webroot/check/.htpasswd:
  file.managed:
    - source: salt://asalae/htpasswd

/etc/httpd/conf.d/asalae.conf:
  file.managed:
    - source: salt://asalae/apache-vhost.conf
    - template: jinja

/var/lib/pgsql/9.4/data/pg_hba.conf:
  file.managed:
    - source: salt://asalae/pg_hba.conf
    - template: jinja
    - watch: 
      - pkg: postgresql94


postgres-pkgs:
  pkg:
    - installed
    - pkgs:
      - postgresql94-contrib

postgres_restart: 
  cmd.run:
    - name: service postgresql-9.4 restart > /dev/null 2>&1 

create_user:
 cmd.run:
 - name: cd /tmp && sudo -u postgres bash -c "psql -c \"CREATE USER {{ asalae.db.user }} WITH ENCRYPTED PASSWORD '{{ asalae.db.password }}';\""
 - unless: sudo -u postgres bash -c "psql -c "SELECT * FROM pg_roles where rolname='asalae';""

create_db:
 cmd.run:
 - name: cd /tmp/ && sudo -u postgres bash -c "psql -c \"CREATE DATABASE {{ asalae.db.database }} ENCODING 'UTF8' OWNER {{ asalae.db.user }};\""
 - unless: sudo -u postgres bash -c "psql -c "SELECT * FROM pg_database where datname='asalae';""


inject_schema:
  cmd.run:
    - name: cd /tmp && psql -U {{ asalae.db.user }} {{ asalae.db.database }} < /var/www/asalae/app/config/sql/postgresql/asalae_postgres_1.6.0.sql
    - unless: sudo -u postgres bash -c "psql -c "SELECT * FROM pg_tables where tablename='acos';""
    

create_extension:
  cmd.run:
    - name: cd /tmp && sudo -u postgres bash -c "psql -c \"CREATE EXTENSION unaccent;\""
    - unless: sudo -u postgres bash -c "psql -c "SELECT * FROM pg_extension where extname='unaccent';""
    
del_pgpass:
  file.absent:
   - name: /root/.pgpass
